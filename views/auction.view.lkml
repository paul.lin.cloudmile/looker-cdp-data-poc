view: auction {
  sql_table_name: `kaggle_auction.auction`
    ;;
  drill_fields: [auctionid]

  dimension: auctionid {
    primary_key: yes
    type: number
    sql: ${TABLE}.auctionid ;;
  }

  dimension: bid {
    type: number
    value_format_name: id
    sql: ${TABLE}.bid ;;
  }

  dimension: bid_duetime {
    type: number
    sql: ${TABLE}.bid_duetime ;;
  }

  dimension: bidder {
    type: string
    sql: ${TABLE}.bidder ;;
  }

  dimension: bidderrate {
    type: number
    sql: ${TABLE}.bidderrate ;;
  }

  dimension: bidtime {
    type: number
    sql: ${TABLE}.bidtime ;;
  }

  dimension: final_price {
    type: number
    sql: ${TABLE}.final_price ;;
  }

  dimension: finalprice_diff {
    type: number
    sql: ${TABLE}.finalprice_diff ;;
  }

  dimension: item {
    type: string
    sql: ${TABLE}.item ;;
  }

  dimension: openbid {
    type: number
    value_format_name: id
    sql: ${TABLE}.openbid ;;
  }

  measure: count {
    type: count
    drill_fields: [auctionid, auction_info.count, auction_more.count]
  }
}
