view: auction_info {
  sql_table_name: `ebaybid.auction_info`
    ;;

  dimension: auctionid {
    type: number
    value_format_name: id
    # hidden: yes
    sql: ${TABLE}.auctionid ;;
  }

  dimension: bid_count {
    type: number
    sql: ${TABLE}.bid_count ;;
  }

  dimension: bid_duetime {
    type: number
    sql: ${TABLE}.bid_duetime ;;
  }

  dimension: final_price {
    type: number
    sql: ${TABLE}.final_price ;;
  }

  dimension: item {
    type: string
    sql: ${TABLE}.item ;;
  }

  dimension: openbid {
    type: number
    value_format_name: id
    sql: ${TABLE}.openbid ;;
  }

  measure: count {
    type: count
    drill_fields: [auction.auctionid]
  }

  measure: total_price {
    type: sum
    drill_fields: [auction.final_price]
    sql:  ${TABLE}.final_price ;;
  }

  measure: avg_bidcount {
    type: average
    drill_fields: [auction.bid_count]
    sql:  ${TABLE}.bid_count ;;
  }

  measure: med_open_bid {
    type: median
    drill_fields: [auction.openbid]
    sql:  ${TABLE}.openbid ;;
  }

  measure: med_final_price {
    type: median
    drill_fields: [auction.final_price]
    sql:  ${TABLE}.final_price ;;
  }

  measure: max_final_price {
    type: max
    drill_fields: [auction.final_price]
    sql:  ${TABLE}.final_price ;;
  }

  measure: min_final_price {
    type: min
    drill_fields: [auction.final_price]
    sql:  ${TABLE}.final_price ;;
  }

  measure: avg_bid_duetime {
    type:  average
    drill_fields: [auction.bid_duetime]
    sql:  ${TABLE}.bid_duetime ;;
  }
}
