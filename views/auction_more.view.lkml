view: auction_more {
  sql_table_name: `ebaybid.auction_more`
    ;;

  dimension: auctionid {
    type: number
    value_format_name: id
    # hidden: yes
    sql: ${TABLE}.auctionid ;;
  }

  dimension: bid {
    type: number
    value_format_name: id
    sql: ${TABLE}.bid ;;
  }

  dimension: bid_duetime {
    type: number
    sql: ${TABLE}.bid_duetime ;;
  }

  dimension: bidder {
    type: string
    sql: ${TABLE}.bidder ;;
  }

  dimension: bidderrate {
    type: number
    sql: ${TABLE}.bidderrate ;;
  }

  dimension: bidtime {
    type: number
    sql: ${TABLE}.bidtime ;;
  }

  dimension: final_price {
    type: number
    sql: ${TABLE}.final_price ;;
  }

  dimension: finalprice_diff {
    type: number
    sql: ${TABLE}.finalprice_diff ;;
  }

  dimension: item {
    type: string
    sql: ${TABLE}.item ;;
  }

  dimension: openbid {
    type: number
    value_format_name: id
    sql: ${TABLE}.openbid ;;
  }

  dimension: price_diffratio {
    type: number
    sql: ${TABLE}.price_diffratio ;;
  }

  dimension: remain_mins {
    type: number
    sql: ${TABLE}.remain_mins ;;
  }

  measure: count {
    type: count
    drill_fields: [auction.auctionid]
  }

  measure: avg_diffpriceratio {
    type:  average
    drill_fields: [auction.price_diffratio]
    sql:  ${TABLE}.price_diffratio ;;
  }

  measure: avg_diffprice {
    type:  average
    drill_fields: [auction.finalprice_diff]
    sql:  ${TABLE}.finalprice_diff ;;
  }

  measure: avg_remaintime {
    type:  average
    drill_fields: [auction.remain_mins]
    sql:  ${TABLE}.remain_mins/60/24 ;;
  }

}
