connection: "cdp-poc-bq"
# connection: "ml-team-connector"

# include all the views
include: "/views/**/*.view"

datagroup: cdp_poc_bigquery_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: cdp_poc_bigquery_default_datagroup

explore: auction{}

# explore: auction_info {
#   join: auction {
#     type: left_outer
#     sql_on: ${auction_info.auctionid} = ${auction.auctionid} ;;
#     relationship: many_to_one
#   }
# }

# explore: auction_more {
#   join: auction {
#     type: left_outer
#     sql_on: ${auction_more.auctionid} = ${auction.auctionid} ;;
#     relationship: many_to_one
#   }
# }
